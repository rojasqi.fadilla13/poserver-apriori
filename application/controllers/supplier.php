<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;
 
class Supplier extends REST_Controller {
 
    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }
 
    // show data supplier
    function index_get() {
        $sid = $this->get('sid');
        if ($sid == '') {
            $supplier = $this->db->get('suplier')->result();
        } else {
            $this->db->where('sid', $sid);
            $supplier = $this->db->get('suplier')->result();
        }
        $this->response($supplier, 200);
    }
 
    // insert data supplier
    function index_post() {
        $data = array(
                    'sid'     	=> $this->post('sid'),
            		'snama'		=>$this->input->post('snama'),
            		'salamat'	=>$this->input->post('salamat'),
            		'stelp'		=> $this->input->post('stelp'),
                    );
        $insert = $this->db->insert('suplier', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
 
    // update data supplier
    function index_put() {
        $sid = $this->put('sid');
        $data = array(
                    'sid'       => $this->put('sid'),
            		'snama'		=>$this->input->post('snama'),
            		'salamat'	=>$this->input->post('salamat'),
            		'stelp'		=> $this->input->post('stelp'),
                    );
        $this->db->where('sid', $sid);
        $update = $this->db->update('suplier', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
 
    // delete data supplier
    function index_delete() {
        $sid = $this->delete('sid');
        $this->db->where('sid', $sid);
        $delete = $this->db->delete('suplier');
        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
 
}